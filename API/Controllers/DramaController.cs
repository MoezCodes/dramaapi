﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using Swashbuckle.Swagger.Annotations;
using System.Net;
using System.Net.Http;
using SendGrid.Helpers.Mail;
using SendGrid;
using System.Net.Mail;
using System.Net.Mime;
using System;
using System.Text;
using System.IO;
using SelectPdf;
using System.Configuration;
using System.Text.RegularExpressions;
using DaramaAPI.Models;
using System.Net.Http.Formatting;

namespace DaramaAPI.Controllers
{
    public class DramaController : ApiController
    {

 
        [HttpPost]
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<Models.Response>))]
        [Route("~/")]
        public IHttpActionResult FilterDramas(Programs req )
        {
            Models.Response response = new Models.Response();
            List<Drama> dramas = new List<Drama>();
            try
            {
               
                var payloads = req.payload.Where(x => x.drm == true && x.episodeCount > 0).ToList();

                for (int i = 0; i < payloads.Count; i++)
                {
                    Drama r = new Drama();
                    r.image = payloads[i].image.ShowImage;
                    r.title = payloads[i].Title;
                    r.slug = payloads[i].Slug;
                    dramas.Add(r);
                }

                response.response = dramas;
                return Json(response);
            }
            catch (Exception ex)
            {
                IHttpActionResult res;
                 
                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.BadRequest);
                responseMsg.Content = new ObjectContent<ErrorResponse>(
                        new ErrorResponse()
                        {
                            error = "Could not decode request: JSON parsing failed"
                        },
                        new JsonMediaTypeFormatter(), "application/json");
                res = ResponseMessage(responseMsg);

                return res;
               
            }

        }

      
    }

}
