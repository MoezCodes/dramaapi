﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DaramaAPI.Models
{
    //TODO: Split classes into different files
    public class Image
    {
        public string ShowImage { get; set; }
    }

    public class NextEpisode
    {
        public object channel { get; set; }
        public string channelLogo { get; set; }
        public object date { get; set; }
        public string html { get; set; }
        public string url { get; set; }
    }

    public class Season
    {
        public string slug { get; set; }
    }

    public class Payload
    {
        public string country { get; set; }
        public string description { get; set; }
        public bool drm { get; set; }
        public int episodeCount { get; set; }
        public string genre { get; set; }
        public Image image { get; set; }
        public string Language { get; set; }
        public NextEpisode nextEpisode { get; set; }
        public string primaryColour { get; set; }
        public List<Season> seasons { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string TvChannel { get; set; }
    }

    public class Programs
    {
        public List<Payload> payload { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public int TotalRecords { get; set; }
    }
    public class Drama
    {
        public string image { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
        
    }
    public class Response
    {
        public List<Drama> response { get; set; }

    }
    public class ErrorResponse
    {
        public string error { get; set; }

    }


}